import sys

def main():
  counts = {}
  for line in sys.stdin:
    for c in line:
      if c not in counts:
        counts[c] = 0
      counts[c] += 1

  print(counts['A'], counts['C'], counts['G'], counts['T'])

main()
