from collections import defaultdict
import util
import sys

def main():
  for seq_id, seq in util.parse_fasta(sys.stdin):
    seq_counts = defaultdict(int)
    for nt in seq:
      seq_counts[nt.upper()] += 1
    print(seq_id)
    gc_content = (seq_counts['G'] + seq_counts['C']) / sum(seq_counts.values())
    print('%.6f' % (100 * gc_content))

main()
