from collections import defaultdict
import sys
import util

def main():
  overlap_len = 3

  seqs = [seq for seq in util.parse_fasta(sys.stdin)]

  for seq1_id, seq1 in seqs:
    for seq2_id, seq2 in seqs:
      if seq1_id == seq2_id:
        continue
      if seq1[-overlap_len:] == seq2[:overlap_len]:
        print(seq1_id, seq2_id) # Edge from seq1 -> seq2

main()
