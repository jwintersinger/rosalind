import sys

def main():
  seqs = []
  for line in sys.stdin:
    seqs.append(line.strip())

  assert len(seqs) == 2
  seq1, seq2 = seqs
  assert len(seq1) == len(seq2)

  hamming = sum([a != b for a, b in zip(seq1, seq2)])
  print(hamming)

main()
    

