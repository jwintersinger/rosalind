#!/usr/bin/python
import sys
import util

def is_valid_motif(motif, seqs):
  '''
  Returns True if motif is present in every seq in seqs, False otherwise.
  '''
  for seq in seqs:
    if motif not in seq:
      return False
  # We fell through to this point, so motif must be in every seq.
  return True

def main():
  # Parse FASTA file. seqs will contain all our sequences (e.g., seqs = ['ACT', 'TGA']).
  seqs = [seq[1] for seq in util.parse_fasta(sys.stdin)]
  # Use first_seq as source for motif. By definition, the motif we find will be
  # present in *every* sequence, including the first one, so using first_seq as
  # the source will result in us finding the correct answer.
  first_seq = seqs.pop()
  # Record longest motif we've found thus far.
  longest_motif = ''

  # Idea: test each subsequence in first_seq to see if it's present in all
  # other sequences. If it is, it's a motif!
  # Suppose first_seq is ACGTA. Then, generate motifs thusly:
  #   1a. Is ACGTA present in every seq? (start with whole possible sequence)
  #   1b. Is ACGT present in every seq?  (chop off last character)
  #   1c. Is ACG present in every seq?   (chop off last character)
  #   1d. Is AC present in every seq?    (chop off last character)
  #   1e. Is A present in every seq?     (chop off last character)
  #   2a. Is CGTA present in every seq?  (Start over, but remove the first character.)
  #   2b. Is CGT present in every seq?   (chop off last character)
  #   2c. Is CG present in every seq?    (chop off last character)
  #   2d. Is C present in every seq?     (chop off last character)
  #   3a. Is GTA present in every seq?   (Start over, but remove the first character.)
  #   3b. Is GT present in every seq?    (chop off last character)
  #   3c. Is G present in every seq?     (chop off last character)
  #   4a. Is TA present in every seq?    (Start over, but remove the first character.)
  #   4b. Is T present in every seq?     (chop off last character)
  #   5a. Is A present in every seq?     (Start over, but remove the first character.)

  # m ranges from 0 up to length of first_seq. E.g., if first_seq = 'ACGTT', m ε [0, 1, 2, 3, 4].
  for m in range(len(first_seq)):
    # n ranges from (length of first_seq - 1) down to (m + 1). E.g., if first_seq = 'ACGTT' and m = 1,
    # n ε [4, 3, 2].
    for n in range(len(first_seq) - 1, m + 1, -1):
      cand_motif = first_seq[m:n]
      if is_valid_motif(cand_motif, seqs):
        # Rename variable -- cand_motif is no longer a candidate, but is now a verified motif.
        motif = cand_motif
        if len(motif) > len(longest_motif):
          longest_motif = motif
        # Note that we can optimize our search -- if we find a motif, we can
        # stop chopping characters off its end and testing the resulting
        # strings, because for a given motif, we know that chopping characters
        # off the end will result in strings that are also motifs, but that are
        # shorter than the one we found.
        break

  print(longest_motif)

main()
