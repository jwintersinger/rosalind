#!/usr/bin/python
import sys
import util

def is_valid_motif(motif, seqs):
  '''
  Returns True if motif is present in every seq in seqs, False otherwise.
  '''
  for seq in seqs:
    if motif not in seq:
      return False
  return True

def main():
  seqs = [seq[1] for seq in util.parse_fasta(sys.stdin)]
  first_seq = seqs.pop()

  for size in range(len(first_seq), 1, -1):
    for start in range(len(first_seq) - size + 1):
      end = start + size
      candidate = first_seq[start:end]
      if is_valid_motif(candidate, seqs):
        print(candidate)
        sys.exit()

main()
