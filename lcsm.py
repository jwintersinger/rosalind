#!/usr/bin/python
import sys
import util
from collections import defaultdict

valid_motifs = defaultdict(set)

def perm(prefix):
  alphabet = ('A', 'C', 'G', 'T')
  for char in alphabet:
    yield prefix + char

def in_all_seqs(motif, seqs):
  for seq in seqs:
    if motif not in seq:
      #print('%s not in %s' % (motif, seq))
      return False
  return True

def explore(prefix, seqs):
  if in_all_seqs(prefix, seqs):
    for candidate in perm(prefix):
      explore(candidate, seqs)
    valid_motifs[len(prefix)].add(prefix)

def main():
  seqs = [seq[1] for seq in util.parse_fasta(sys.stdin)]
  explore('', seqs)
  longest_motif = max(valid_motifs.keys())
  print(valid_motifs[longest_motif].pop())

main()
