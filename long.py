import util
import sys

def align(seq1, seq2):
  m, n = len(seq1), len(seq2)
  a, b = m, m
  c, d = 0, 0
  aligned = seq1 + seq2

  while not (a < 0 or d > n):
    if seq1[a:b] == seq2[c:d]:
      candidate_alignment = seq1 + seq2[d:]
      if len(candidate_alignment) < len(aligned):
        aligned = candidate_alignment
    a -= 1
    d += 1

  return aligned

def calc_score(seq1, seq2):
  aligned = align(seq1, seq2)
  score = len(seq1 + seq2) - len(aligned)
  return score

def main():
  seqs = [seq for (seq_id, seq) in util.parse_fasta(sys.stdin)]

  while len(seqs) > 1:
    n = len(seqs)
    max_score = 0
    best_pair = None
    print('%s sequence(s) remain ...' % n)

    for i in range(n):
      for j in range(n):
        if j == i:
          continue
        score = calc_score(seqs[i], seqs[j])
        if score > max_score:
          best_pair = (i, j)
          max_score = score

    if best_pair is None:
      print(len(seqs))
      print(max_score)
      raise Exception('No best_pair')

    a, b = best_pair
    new_seqs = [align(seqs[a], seqs[b])]
    for k in range(n):
      if k not in best_pair:
        new_seqs.append(seqs[k])
    seqs = new_seqs

  print(seqs[0])

main()
