import util
import sys

def find_orfs(seq):
  start_codon = 'ATG'
  stop_codons = ('TAG', 'TAA', 'TGA')

  final_triplet_start = len(seq) - 2
  orfs = []

  for i in range(0, final_triplet_start):
    if seq[i:(i + 3)] == start_codon:
      cds_start = i

      for j in range(cds_start, final_triplet_start, 3):
        if seq[j:(j + 3)] in stop_codons:
          cds = seq[cds_start:j]
          orfs.append(cds)
          break

  return orfs

def translate(seq):
  codon_table = util.generate_codon_table()
  translated = ''
  for i in range(0, len(seq), 3):
    codon = seq[i:(i + 3)]
    translated += codon_table[codon]
  return translated

def main():
  for seq_id, seq in util.parse_fasta(sys.stdin):
    orfs = set(find_orfs(seq) + find_orfs(util.reverse_complement(seq)))
  for orf in orfs:
    print(translate(orf))

main()
