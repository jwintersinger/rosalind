import util
import sys

def main():
  rna_seq = sys.stdin.read().strip()
  rna_seq = rna_seq.replace('\n', '')
  dna_seq = rna_seq.replace('U', 'T')

  translated = ''
  codon_table = util.generate_codon_table()
  stop_codons = ('TAA', 'TAG', 'TGA')

  for i in range(0, len(dna_seq), 3):
    codon = dna_seq[i:(i + 3)]
    if codon in stop_codons:
      break
    translated += codon_table[codon]
  print(translated)

main()
