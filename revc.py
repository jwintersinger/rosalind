import sys

def main():
  query_seq = ''.join(sys.stdin.readlines())
  rev_query_seq = query_seq[::-1]
  for c in rev_query_seq:
    print(complement(c), end='')
  print()

def complement(char):
  complements = {
    'A': 'T',
    'C': 'G',
    'G': 'C',
    'T': 'A'
  }
  if char in complements:
    return complements[char]
  return ''

main()
