import sys

def main():
  seq = ''
  for line in sys.stdin:
    line = line.strip()
    if line.startswith('>'):
      continue
    seq += line

  seq = 'ACGTTGCA'

  for plength in range(4, 12 + 1):
    start = 0
    while start + 2*plength < len(seq):
      if seq[start:(start + plength)] == seq[(start + plength):(start + 2*plength)][::-1]:
        print('Pants', seq[start:(start + plength)], seq[(start + plength):(start + 2*plength)][::-1])
        print(start + 1, plength)
      start += 1

main()
