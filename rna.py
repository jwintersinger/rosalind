import sys

def main():
  for line in sys.stdin:
    for c in line:
      if c == 'T':
        print('U', end='')
      else:
        print(c, end='')

main()
