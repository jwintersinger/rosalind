import sys

def main():
  haystack = sys.stdin.readline().strip()
  needle = sys.stdin.readline().strip()

  for i in range(len(haystack)):
    if haystack[i:(i + len(needle))] == needle:
        print(i + 1, end=' ')
  print()

main()
